// alert("Hello!")





/*
	OBJECTS
		an object is a data type that is used  to rpesent real world objects. It is also a collection of related data and /or functionalities.

	Creating objects using literal
	Syntax:
		let objectName = {
			keyA: valueA,
			keyB: valueB
		}

*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
}

console.log("Result of creating objects using object literal:")
console.log(cellphone)
console.log(typeof cellphone)


// Creating objects using a constructor function
/*
	Create a reusable function to create a several objects that have the same data structure. This is useful for creating multiple instances/copies of an object.

	We don't use camel case here.

	Syntax:
		function ObjectName(valueA, valueB){
			this.keyA = valueA
			this.keyB = valueB
		}

		variable = new function ObjectName(valueA, valueB);
		console.log(variable)
		-this is for invoking
		-don't forget the "new" keyword when creating the variable

*/

function Laptop (name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate;
}

let laptop = new Laptop("Lenovo", 2008);
console.log("Result of creating objects using object constructors:");
console.log(laptop);

let myLaptop = new Laptop("MacBook Air", 2020);
console.log(myLaptop);



let oldLaptop = Laptop("Acer", 2008);
console.log("Result of creating objects without the new keyword:");
console.log(oldLaptop);


// Creating an empty object
// first:
let computer = {}
// second:
let myComputer = new Object();

console.log(computer);
console.log(myComputer);

// Accessing Object Property

// Using the dot notation
console.log("Result from dot notation: " + myLaptop.name);

// Using the square bracket notation
console.log("Result from dot notation: " + myLaptop["name"]);

// Accessing array objects

let array = [laptop, myLaptop];
// let array = [{name: Lenovo, manufactureDate: 2008 }, {name: MacBook Air, manufactureDate: 2020}]

// Using the square bracket notation
console.log(array[0]["name"]);
// Using the dot notation
console.log(array[0].name);


// Initializing/Adding/Deleting/Reassigning Object Properties


// Adding object properties
let car = {}
console.log(car)

car.name = "Honda Civic"
console.log("Result from adding propery using the dot notation:")
console.log(car)

// square bracket notation is ideal when property name requires more than 2 words with space
car["manufacture date"] = 2019
console.log("Result from adding propery using the square bracket notation:")
console.log(car)

// Deleting object properties
delete car["manufacture date"];
console.log("Result from property propery using the square bracket notation:")
console.log(car)

// Reassigning object properties
car.name = "Tesla";
console.log("Result from reassigning propery value:");
console.log(car);


// Object Methods
/*
	A method is a function which is a property of an object. They are also functions and one of the key differences they have is that methods are functions related to a specific object.

*/


let person = {
	name: "John",
	talk: function () {
		console.log("Hello! My name is " + this.name);
	}
} 

console.log(person);
console.log("Result from object methods: ");
person.talk();

person.walk = function() {
	console.log(this.name + " walked 25 steps forward.")
}
person.walk();


let friend = {
	firstName: "Nehemiah",
	lastName: "Ellorico",
	address: {
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["nejellorico@gmail.com", "nej123@gmail.com"],
	introduce: function() {
		console.log("Hello! My name is " + this.firstName + " " + this.lastName);
	}
}

friend.introduce();

// Real World Application of Objects data type

/*

	Scenario:
		1. We would like to create a game that would have several pokemon interact with each other.
		2. every pokemon would have the same set of stats, properties and functions.

*/

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to targetPokemonHealth");

	},
	faint: function() {
		console.log("Pokemon fainted");
	}
}

console.log(myPokemon)


// Creating an object constructor

function Pokemon (name, level) {
	// properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	// methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
	},
	this.faint = function() {
		console.log(this.name + "fainted.")
	}
}

let pikachu = new Pokemon("Pikachu", 16);
let squirtle = new Pokemon("Squirtle", 8)

console.log(pikachu)
console.log(squirtle)

pikachu.tackle(squirtle);
pikachu.tackle(squirtle);


/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function


*/


function Pokemon (name, level) {
	// properties
	this.name = name;
	this.level = level;
	this.health = 40;
	this.attack = 12;

	this.tackle = function(target) {

		let newHealt = target.health - this.attack
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
		if (newHealt <= 5) {
			(target).faint()
		} else {
			newHealt = target.health
		}

	},
	this.faint = function(target) {
		console.log(this.name + " fainted.")
	}
}

let eevee = new Pokemon("Eevee", 5);
let charizard = new Pokemon("Charizard", 8)

console.log(eevee)
console.log(charizard)

eevee.tackle(charizard);
charizard.health = (charizard.health - eevee.attack);
eevee.tackle(charizard);
charizard.health = (charizard.health - eevee.attack);
eevee.tackle(charizard);
charizard.health = (charizard.health - eevee.attack);
eevee.tackle(charizard);
charizard.health = (charizard.health - eevee.attack);


console.log(eevee)
console.log(charizard)


/*function charizardHealth () {
	if (charizard.health <= 5)
		charizard.faint()
}	

charizardHealth()
*/